# online-shop
https://online-shop-ashen-seven.vercel.app/products  
## Install npm packages
Install the npm packages described in the package.json and verify that it works:

`npm install`

`npm start`

Start JSON Server

`json-server --watch db.json`

## Description
This project is a web application for managing products using Angular Material and JSON Server.
The application provides the following features:

* Adding new product cards.
* Editing existing product cards.
* Adding products to the shopping cart.

## Technologies
* Angular Material: Used for creating the user interface of the application, providing a modern and responsive design.
* JSON Server: Used to simulate the server-side of the application and store product data.
## Features
* Adding Product Cards: Users can add new product cards to the system, specifying details like product name, description, price, and image.
* Editing Product Cards: Existing product cards can be edited with updated information.
* Shopping Cart: Users can add products to their shopping cart for future purchase or reference.



