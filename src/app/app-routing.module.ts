import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BaseComponent} from "./components/base/base.component";
import {ProductsComponent} from "./components/products/products.component";
import {ProductDetailsComponent} from "./components/product-details/product-details.component";
import {BasketComponent} from "./components/basket/basket.component";
import {productResolver} from "./services/product.resolver";

const routes: Routes = [
  {path: 'products', component: ProductsComponent},
  {path: '', redirectTo: 'products', pathMatch: 'full'},

  {
    path: 'product/:id', component: ProductDetailsComponent,
    resolve: {
      data: productResolver
    }
  },
  {path: 'basket', component: BasketComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
