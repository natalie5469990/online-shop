import {Component, OnDestroy, OnInit} from '@angular/core';
import {Product} from "../../models/product";
import {Subject, takeUntil} from "rxjs";
import {ProductsService} from "../../services/products.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {DialogBoxComponent} from "../dialog-box/dialog-box.component";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {
  public products!: Product[];
  public canEdit: boolean = true;
  public basket!: Product[];
  private destroy$: Subject<boolean> = new Subject();

  constructor(private ProductsService: ProductsService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.ProductsService.getProducts().pipe(
      takeUntil(this.destroy$)).subscribe((data: Product[]) => {
      this.products = data;
    });

    this.ProductsService.getProductBasket().pipe(
      takeUntil(this.destroy$)).subscribe((data: Product[]) => {
      this.basket = data;
    })
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  public openDialog(product?: Product): void {
    let dialogConfig = new MatDialogConfig();
    dialogConfig.width = '500px';
    dialogConfig.disableClose = true;
    dialogConfig.data = product;

    const dialogRef = this.dialog.open(DialogBoxComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((data) => {
      if (data) {
        if (data && data.id) {
          this.updateData(data);
        } else {
          this.postData(data);
        }
      }
    });
  }

  public deleteItem(id: number): void {
    this.ProductsService.deleteProduct(id).subscribe(() => {
      const index = this.products.findIndex(item => item.id === id);
      if (index !== -1) {
        this.products.splice(index, 1);
      }
    });
  }

  public addToBasket(product: Product): void {
    product.quantity = 1;
    let findItem;
    if (this.basket.length > 0) {
      findItem = this.basket.find((item) => item.id === product.id);
      if (findItem) {
        this.updateBasket(findItem);
      } else {
        this.postBasket(product)
      }
    } else {
      this.postBasket(product);
    }
  }

  private updateData(product: Product): void {
    this.ProductsService.updateProduct(product).subscribe((data: Product) => {
      this.products = this.products.map((product) => {
        if (product.id === data.id) {
          return data;
        } else {
          return product;
        }
      })
    });
  }

  private postData(data: Product): void {
    this.ProductsService.postProduct(data).subscribe((data: Product) => this.products.push(data));
  }

  private postBasket(product: Product): void {
    this.ProductsService.postProductToBasket(product).subscribe((data: Product) => {
      this.basket.push(data);
    });
  }

  private updateBasket(product: Product): void {
    product.quantity! += 1;
    this.ProductsService.updateProductBasket(product).subscribe();
  }

}
