import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-dialog-box',
  templateUrl: './dialog-box.component.html',
  styleUrls: ['./dialog-box.component.scss']
})
export class DialogBoxComponent {
  public isNew: boolean = true;

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DialogBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    if (this.data){
      this.isNew = false;
    }
  }

  myForm : FormGroup = this.fb.group({
    id: [this.data?.id ?? null],
    title: [this.data?.title ?? null, [Validators.required, Validators.maxLength(30)]],
    price: [this.data?.price ?? null, [Validators.required]],
    year: [this.data?.year ?? null, [Validators.required]],
    chip: [this.data?.configure.chip ?? null, [Validators.required]],
    ssd: [this.data?.configure.ssd ?? null, [Validators.required]],
    memory: [this.data?.configure.memory ?? null, [Validators.required]],
    display: [this.data?.configure.display ?? null, [Validators.required]],
  })

  onNoClick(): void {
    this.dialogRef.close(null);
  }

  onSubmit(): void {
      this.data = {
        id: this.myForm.value.id,
        title: this.myForm.value.title,
        price: this.myForm.value.price,
        year: this.myForm.value.year,
        image: "assets/images/28521-3-macbook.png",
        configure: {
          chip: this.myForm.value.chip,
          ssd: this.myForm.value.ssd,
          memory: this.myForm.value.memory,
          display: this.myForm.value.display,
        }
      }
      this.dialogRef.close(this.data);
  }

}
