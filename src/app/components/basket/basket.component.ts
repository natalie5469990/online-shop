import {Component, OnDestroy, OnInit} from '@angular/core';
import {Product} from "../../models/product";
import {ProductsService} from "../../services/products.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit, OnDestroy {
  public basket!: Product[];
  private basketSubscription!: Subscription;
  constructor(private productService: ProductsService) {}
 ngOnInit() {
   this.basketSubscription = this.productService.getProductBasket().subscribe((data)=>{
      this.basket = data;
    })
 }

 ngOnDestroy() {
    if(this.basketSubscription){
      this.basketSubscription.unsubscribe();
    }
 }

 minusItem(item: Product){
    if (item.quantity === 1){
      this. productService.deleteProductFromBasket(item.id).subscribe((data)=>{
        let index = this.basket.findIndex((data)=> data.id === item.id);
        this.basket.splice(index,1)
      })
    }else{
      item.quantity! -=1;
      this.productService.updateProductBasket(item).subscribe((data)=> {

      })
    }

 }

 plusItem(item: Product){
   item.quantity! +=1;
   this.productService.updateProductBasket(item).subscribe()



   }

}
