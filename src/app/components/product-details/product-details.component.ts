import {Component, OnDestroy, OnInit} from '@angular/core';
import {Product} from "../../models/product";
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
product!: Product;
productSubscription!: Subscription;

constructor(private route: ActivatedRoute) {}
  ngOnInit() {
  this.productSubscription = this.route.data.subscribe((data)=> {
    this.product = data['data'];
  })
  }

  ngOnDestroy() {
    if (this.productSubscription) {
      this.productSubscription.unsubscribe();
    }
  }
}
