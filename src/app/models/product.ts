export interface Product {
  id: number,
  title: string,
  price: number,
  year: number,
  image?:string,
  configure: ProductsConfig;
  quantity?: number;
}
export interface ProductsConfig {
  chip: string,
  SSD: string,
  memory: string,
  display: string
}
