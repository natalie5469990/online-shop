import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../models/product";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
private URL: string = 'http://localhost:3000/products';
private URL_BASKET: string = 'http://localhost:3000/basket';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<Product[]> {
   return this.http.get<Product[]>(this.URL);
  }

  getProduct(id: number): Observable<Product> {
    return  this.http.get<Product>(`${this.URL}/${id}`);
  }

  postProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(this.URL, product);
  }

  deleteProduct(id: number): Observable<{}> {
    return this.http.delete(`${this.URL}/${id}`);
  }

  updateProduct(product: Product):  Observable<Product> {
    return this.http.put<Product>(`${this.URL}/${product.id}`,product);
  }

  getProductBasket(): Observable<Product[]> {
    return this.http.get<Product[]>(this.URL_BASKET);
  }

  postProductToBasket(product: Product):  Observable<Product> {
    return this.http.post<Product>(this.URL_BASKET, product);
  }

  updateProductBasket(product: Product): Observable<Product> {
    return this.http.put<Product>(`${this.URL_BASKET}/${product.id}`,product);
  }

  deleteProductFromBasket(id: number): Observable<{}>{
    return this.http.delete(`${this.URL_BASKET}/${id}`);
  }
}
