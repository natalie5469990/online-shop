import {ResolveFn, Router} from '@angular/router';
import {catchError, EMPTY, Observable} from "rxjs";
import {Product} from "../models/product";
import {ProductsService} from "./products.service";
import {inject} from "@angular/core";

export const productResolver: ResolveFn<Product> = (route,
                                                    state,
): Observable<Product> => {
  const productService: ProductsService = inject(ProductsService);
  const router: Router = inject(Router);

  return productService.getProduct(route.params?.['id']).pipe(
    catchError(() => {
      router.navigate(['products'])
      return EMPTY;
    })
  )
};
